<?php

class First extends TestCase{

    public function setUp(): void {
        $this->resetInstance();
    }


    function testContaDeveInserirRegistroCorretamente(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 12, 2021);

        $this->assertEquals('Casas Bahia', $res[0]['parceiro']);
        $this->assertEquals(2021, $[0]['ano']);
        $this->assertEquals(1, $res[0][mes']);
        $this->assertEquals(3, sizeof($res));

        // $this->assertEquals('Casas Bahia', $res[1]['parceiro']);
        // $this->assertEquals(2021, $[1]['ano']);
        // $this->assertEquals(1, $res[1][mes']);

        $this->assertEquals('Aluguel', $res[2]['parceiro']);
        $this->assertEquals(97.25, $res['valor']);
        $this->assertEquals(2021, $[2]['ano']);
        $this->assertEquals(1, $res[2][mes']); 
    }

    function testContaDeveInformarTotalDeContasAPagarEAReceber(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);

        $this->assertEquals(5097.25, $res);
    }

    function testContaDeveCalcularSaldoMensal(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(1, 2021);
        $this->assertEquals(-875.07, $res);
    }

}
